# Author: Ronan Thomson-Paressant
# 01 Sep 2017
# ==============================================================================
# Pulls GitLab issues (filtered by label name if desired) and compiles a LaTeX # document with the content.
# ==============================================================================
# Python library imports:
import requests
# Custom library imports:
import URL_Maker



# Only execute the below if this script is not being imported by another module
if __name__ == "__main__":
    # Generates and then submits the user-entered API query
    url = URL_Maker.makeRequestURL('''enter console input here''')
    request = requests.get(url)

    # Prints the response code for the query (i.e. 200 - OK, or 404 - Not Found)
    print(request.status_code)
